<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		body{
		 margin: 0px;
		}

		.container{
		  width: calc(100% - 300px);
		  margin: 0 auto;
		}
		.form{
			margin-bottom: 20px;
		}
		
		input{
		  padding: 15px;
		  margin: 5px 0 22px 0;
		  display: block;
		  Width: 30%;
		  border-color: #E6E7F4;
		  border: 1px solid #A4A4AB;
		  border-radius: 20px;
		}
		
		.bt1{
		  padding: 10px 10px;
		  border: 1px solid black;
		  outline: none;
		}

	</style>
</head>
<body>
    <header><?php include("header.php"); ?></header>
    <section class="container">
		<div class="form">
      <center><form>
		<h1>Contact Us</h1>

        <label>Full Name</label>
        <input type="text" placeholder="Enter Name" name="name" required>

        <label>Email</label>
        <input type="email" placeholder="Enter email" name="mail" required>

        <label>Phone Number</label>
        <input type="tel" placeholder="Enter Phone Number" name="phone" required>
     
	    <label>Skype Id</label>
        <input type="text" placeholder="Enter Skype Id" name="id" required>
   
        <button type="button" class="bt1">Send</button>
      </form></center>
    </div>
  </section>
    <?php include("footer.php"); ?>
</body>
</html>