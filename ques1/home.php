<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link type="text/css" rel="stylesheet" href="./css/home.css">
</head>
<body>
    <?php include("header.php"); ?>
    <section class="container">
         <div class="passion">
             <div class="img-finder">
                 <img src="./images/passion.png" alt="an image">
             </div>
             <div class="text">
                 <p class="bigText">
                     Find Your Passion and achieve Success
                 </p>
                 <p class="smallText">
                     Find a Job that suits your interest and talents. A high Salary is not the top priority. Most importantly, you can work according to your heart's desire.
                 </p>.
             </div>
         </div>
     </section>
     
     <section class="container">
         <div class="passion">
             <div class="text">
                 <p class="bigText">
                     Millions of job, find the one that rights for you
                 </p>
                 <p class="smallText">
                     Analytical Bootstraping buzz first mover advantage network effects funding handshakes. Buyer Social media burn rate interaction design prototype handshake.
                 </p>.
             </div>
             <div class="img-finder">
                 <img src="./images/finder.png" alt="an image">
             </div>
         </div>
     </section>
    <?php include("footer.php"); ?>
</body>
</html>