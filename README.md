Assignment Question are as follows:

1. Make a website using include or require in php. Follow the folder structure explained in today's session. Make seperate file for footer and header. Website will consist of following pages.
- Main page (Introduction about company)
- Team (Team members)
- Services (Web Development, App development, SEO, Digital Marketing)
- Contact Us (Form with fields - Name, Contact No, Message)

Tip: Use all the HTML & CSS knowledge you can to make this website beautiful.

2. Make a Login app for Admin. Admin will enter username and password. On authentication, Admin will be redirected on his dashboard. Admin will have the option to logout as well.

3. Make a web app that does following

- Form to upload a txt file. Save this txt file's name in db.
- Listing of uploaded files.
- Link to read details

4. Make a web app that does following
i. Admin Module
- Login system for Admin.
- Admin will goto his dashboard (Employees listing)
- Admin will be able to add, edit & delete employee(s) [Fields- Name, Rollno, Email, Profile picture].
- Logout

ii. Employee Module
- Login system for Employee.
- Employee will be able to see his/her details on dashboard.
- Logout 
